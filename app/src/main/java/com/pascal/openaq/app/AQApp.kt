package com.pascal.openaq.app

import android.app.Application
import com.pascal.openaq.di.AppComponent
import com.pascal.openaq.di.AppModule
import com.pascal.openaq.di.DaggerAppComponent
import com.pascal.openaq.di.RemoteModule


class AQApp : Application() {
    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initializeDagger()
    }

    fun initializeDagger() {

        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .remoteModule(RemoteModule()).build()
    }
}