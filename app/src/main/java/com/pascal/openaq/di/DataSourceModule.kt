package com.pascal.openaq.di

import com.pascal.openaq.city.network.Repository
import com.pascal.openaq.city.network.model.RemoteAQService
import dagger.Module
import dagger.Provides
import java.util.concurrent.Executors
import javax.inject.Singleton

@Module
class DataSourceModule {
    @Provides
    @Singleton
    fun provideRepository(remoteAQService: RemoteAQService) = Repository(remoteAQService, Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()))


}
