package com.pascal.openaq.di

import android.app.Application
import android.content.Context
import com.pascal.openaq.app.AQApp
import com.pascal.openaq.city.network.Repository
import com.pascal.openaq.city.ui.AQViewModelFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class AppModule(private val aqApplication: AQApp) {

    @Provides
    @Singleton
    fun provideContext(): Context = aqApplication

    @Provides
    @Singleton
    fun provideViewModelFactory(app: Application, repository: Repository): AQViewModelFactory {
        return AQViewModelFactory(app, repository)
    }

    @Provides
    @Singleton
    fun provideApplication(): Application = aqApplication

}
