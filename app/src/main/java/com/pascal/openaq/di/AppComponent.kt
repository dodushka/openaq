package com.pascal.openaq.di

import com.pascal.openaq.city.ui.MainActivity
import dagger.Component
import javax.inject.Singleton


@Component(modules = arrayOf(AppModule::class, RemoteModule::class, DataSourceModule::class))
@Singleton
interface AppComponent {
    fun inject(mainActivity: MainActivity)
}