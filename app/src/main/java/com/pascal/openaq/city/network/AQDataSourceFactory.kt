package com.pascal.openaq.city.network

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.DataSource
import com.pascal.openaq.city.network.model.City
import com.pascal.openaq.city.network.model.RemoteAQService
import java.util.concurrent.Executor

class AQDataSourceFactory(private val api: RemoteAQService, private val retryExecutor: Executor) : DataSource.Factory<Int, City>() {
    val sourceLiveData = MutableLiveData<AQNetworkDataSource>()
    override fun create(): DataSource<Int, City> {
        val source = AQNetworkDataSource(api, retryExecutor)
        sourceLiveData.postValue(source)
        return source
    }
}