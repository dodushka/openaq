package com.pascal.openaq.city.network.model


data class Response(val meta: APIMeta, val results: List<City>) {
    data class APIMeta(val name: String, val found: Int, val page: Int)
}