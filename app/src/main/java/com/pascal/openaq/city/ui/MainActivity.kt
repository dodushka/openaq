package com.pascal.openaq.city.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.pascal.openaq.R
import com.pascal.openaq.app.AQApp
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: AQViewModelFactory
    private lateinit var viewModel: AQViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AQApp.appComponent.inject(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(AQViewModel::class.java)
        initQuery()
    }


    private fun initQuery() {
        val adapter = CityPagedListAdapter({ viewModel.retry() })
        recyclerView.adapter = adapter
        viewModel.startLoading()
        viewModel.cities.observe(this, Observer(adapter::submitList))
        viewModel.networkState.observe(this, Observer {
            adapter.setNetworkState(it)
        })
    }


}
