package com.pascal.openaq.city.network

import android.arch.lifecycle.Transformations
import android.arch.paging.LivePagedListBuilder
import com.pascal.openaq.city.network.model.City
import com.pascal.openaq.city.network.model.RemoteAQService
import java.util.concurrent.Executor

class Repository(private val redditApi: RemoteAQService,
                 private val networkExecutor: Executor) {
    fun getCities(pageSize: Int): Listing<City> {
        val sourceFactory = AQDataSourceFactory(redditApi, networkExecutor)

        val livePagedList = LivePagedListBuilder(sourceFactory, pageSize)
                .setFetchExecutor(networkExecutor)
                .build()

        return Listing(
                pagedList = livePagedList,
                networkState = Transformations.switchMap(sourceFactory.sourceLiveData, {
                    it.networkState
                }),
                retry = {
                    sourceFactory.sourceLiveData.value?.retryAllFailed()
                },
                refresh = {
                    sourceFactory.sourceLiveData.value?.invalidate()
                },
                refreshState = Transformations.switchMap(sourceFactory.sourceLiveData) {
                    it.initialLoad
                }
        )
    }
}