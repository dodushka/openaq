package com.pascal.openaq.city.ui

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import com.pascal.openaq.city.network.Listing
import com.pascal.openaq.city.network.Repository
import com.pascal.openaq.city.network.model.City
import javax.inject.Inject


class AQViewModel @Inject constructor(app: Application, private val repository: Repository) : AndroidViewModel(app) {


    private val repoResult = MutableLiveData<Listing<City>>()
    val cities = Transformations.switchMap(repoResult, { it.pagedList })!!
    val networkState = Transformations.switchMap(repoResult, { it.networkState })!!


    fun startLoading() {
        repoResult.postValue(repository.getCities(PAGED_LIST_PAGE_SIZE))
    }

    fun retry() {
        val listing = repoResult?.value
        listing?.retry?.invoke()
    }


    companion object {
        private const val PAGED_LIST_PAGE_SIZE = 20
    }
}