package com.pascal.openaq.city.ui

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.pascal.openaq.R
import com.pascal.openaq.city.network.model.City
import com.pascal.openaq.city.network.model.NetworkState
import kotlinx.android.synthetic.main.city_item.view.*

class CityPagedListAdapter(private val retryCallback: () -> Unit) : PagedListAdapter<City, RecyclerView.ViewHolder>(diffCallback) {

    private var networkState: NetworkState? = null

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItemViewType(position)) {
            R.layout.city_item -> (holder as CityViewHolder).bindTo(getItem(position))
            R.layout.network_state_item -> (holder as NetworkStateItemViewHolder).bindTo(
                    networkState)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            R.layout.city_item -> CityViewHolder.create(parent)
            R.layout.network_state_item -> NetworkStateItemViewHolder.create(parent, retryCallback)
            else -> throw IllegalArgumentException("unknown view type $viewType")
        }
    }


    private fun hasExtraRow() = networkState != null && networkState != NetworkState.LOADED

    override fun getItemViewType(position: Int): Int {
        return if (hasExtraRow() && position == itemCount - 1) {
            R.layout.network_state_item
        } else {
            R.layout.city_item
        }
    }

    override fun getItemCount(): Int {
        return super.getItemCount() + if (hasExtraRow()) 1 else 0
    }

    fun setNetworkState(newNetworkState: NetworkState?) {
        val previousState = this.networkState
        val hadExtraRow = hasExtraRow()
        this.networkState = newNetworkState
        val hasExtraRow = hasExtraRow()
        if (hadExtraRow != hasExtraRow) {
            if (hadExtraRow) {
                notifyItemRemoved(super.getItemCount())
            } else {
                notifyItemInserted(super.getItemCount())
            }
        } else if (hasExtraRow && previousState != newNetworkState) {
            notifyItemChanged(itemCount - 1)
        }
    }

    class CityViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        fun bindTo(city: City?) {
            itemView.city_name.text = city?.name
            itemView.count.text = city?.count.toString()
        }

        companion object {
            fun create(parent: ViewGroup): CityViewHolder {
                val view = LayoutInflater.from(parent.context)
                        .inflate(R.layout.city_item, parent, false)
                return CityViewHolder(view)
            }
        }

    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<City>() {
            override fun areItemsTheSame(oldItem: City, newItem: City): Boolean = oldItem.name == newItem.name
            override fun areContentsTheSame(oldItem: City, newItem: City): Boolean = oldItem == newItem
        }
    }
}
