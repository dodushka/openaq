package com.pascal.openaq.city.network

import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PageKeyedDataSource
import com.pascal.openaq.city.network.model.City
import com.pascal.openaq.city.network.model.NetworkState
import com.pascal.openaq.city.network.model.RemoteAQService
import com.pascal.openaq.city.network.model.Response
import retrofit2.Call
import java.util.concurrent.Executor


class AQNetworkDataSource constructor(val dataProvider: RemoteAQService, private val retryExecutor: Executor) : PageKeyedDataSource<Int, City>() {


    var orderField: String = "count"
    var sortOrder: String = "desc"
    var minCount: Int = 10000

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, City>) {
        // Ignored, because  we only use @loadAfter()
    }

    private fun filterItems(response: Response?): List<City> {
        return response?.results?.filter { it.count > minCount } ?: emptyList()
    }


    private var retry: (() -> Any)? = null


    val networkState = MutableLiveData<NetworkState>()

    val initialLoad = MutableLiveData<NetworkState>()

    fun retryAllFailed() {
        val prevRetry = retry
        retry = null
        prevRetry?.let {
            retryExecutor.execute {
                it.invoke()
            }
        }
    }


    override fun loadAfter(params: PageKeyedDataSource.LoadParams<Int>, callback: PageKeyedDataSource.LoadCallback<Int, City>) {
        networkState.postValue(NetworkState.LOADING)

        dataProvider.requestCity(orderField, params.requestedLoadSize, sortOrder, params.key).enqueue(
                object : retrofit2.Callback<Response> {
                    override fun onFailure(call: Call<Response>, t: Throwable) {
                        retry = {
                            loadAfter(params, callback)
                        }
                        networkState.postValue(NetworkState.error(t.message ?: "unknown err"))
                    }

                    override fun onResponse(
                            call: Call<Response>,
                            response: retrofit2.Response<Response>) {
                        if (response.isSuccessful) {
                            val data = response.body()
                            retry = null
                            callback.onResult(filterItems(data), nextPage(data?.meta?.page ?: 0))
                            networkState.postValue(NetworkState.LOADED)
                        } else {
                            retry = {
                                loadAfter(params, callback)
                            }
                            networkState.postValue(
                                    NetworkState.error("error code: ${response.code()}"))
                        }
                    }
                }
        )
    }

    fun nextPage(previous: Int): Int = previous + 1

    override fun loadInitial(
            params: PageKeyedDataSource.LoadInitialParams<Int>,
            callback: PageKeyedDataSource.LoadInitialCallback<Int, City>) {
        val request = dataProvider.requestCity(orderField, params.requestedLoadSize, sortOrder, 1)
        networkState.postValue(NetworkState.LOADING)
        initialLoad.postValue(NetworkState.LOADING)


        try {
            val response = request.execute()
            val data = response.body()
            retry = null
            networkState.postValue(NetworkState.LOADED)
            initialLoad.postValue(NetworkState.LOADED)
            callback.onResult(filterItems(data), null, nextPage(data?.meta?.page ?: 0))
        } catch (e: Exception) {
            retry = {
                loadInitial(params, callback)
            }
            val error = NetworkState.error(e.message ?: "unknown error")
            networkState.postValue(error)
            initialLoad.postValue(error)
        }
    }


}