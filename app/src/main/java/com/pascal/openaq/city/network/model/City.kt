package com.pascal.openaq.city.network.model

import com.google.gson.annotations.SerializedName


data class City(
        @SerializedName("city") val name: String,
        val country: String,
        val count: Long
)