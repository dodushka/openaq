package com.pascal.openaq.city.network.model

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface RemoteAQService {

    @GET("cities")
    fun requestCity(
            @Query("order_by") orderField: String,
            @Query("limit") limit: Int,
            @Query("sort") sortOrder: String,
            @Query("page") pageNumber: Int): Call<Response>
}