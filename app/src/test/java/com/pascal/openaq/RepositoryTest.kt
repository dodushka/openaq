/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pascal.openaq

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import android.arch.paging.PagedList
import com.pascal.openaq.city.network.Listing
import com.pascal.openaq.city.network.Repository
import com.pascal.openaq.city.network.model.City
import com.pascal.openaq.city.network.model.NetworkState
import com.pascal.openaq.repository.FakeApi
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import java.util.concurrent.Executor

@RunWith(Parameterized::class)
class RepositoryTest {

    @Suppress("unused")
    @get:Rule // used to make all live data calls sync
    val instantExecutor = InstantTaskExecutorRule()
    private val fakeApi = FakeApi()
    private val networkExecutor = Executor { command -> command.run() }
    private val repository = Repository(fakeApi, networkExecutor)


    @Test
    fun emptyList() {
        val listing = repository.getCities(0)
        val pagedList = getPagedList(listing)
        assertThat(pagedList.size, `is`(0))
    }


    @Test
    fun oneItem() {
        val listing = repository.getCities(1)
        val pagedList = getPagedList(listing)
        assertThat(pagedList.size, `is`(0))
    }

    @Test
    fun networkError() {
        fakeApi.fails = true
        val listing = repository.getCities(1)
        val pagedList = getPagedList(listing)
        assertThat(getNetworkState(listing), `is`(NetworkState.error("Test")))
        fakeApi.fails = false
    }


    private fun getNetworkState(listing: Listing<City>): NetworkState? {
        val networkObserver = LoggingObserver<NetworkState>()
        listing.networkState.observeForever(networkObserver)
        return networkObserver.value
    }

    private fun getPagedList(listing: Listing<City>): PagedList<City> {
        val observer = LoggingObserver<PagedList<City>>()
        listing.pagedList.observeForever(observer)
        assertThat(observer.value, `is`(notNullValue()))
        return observer.value!!
    }

    private class LoggingObserver<T> : Observer<T> {
        var value: T? = null
        override fun onChanged(t: T?) {
            this.value = t
        }
    }
}