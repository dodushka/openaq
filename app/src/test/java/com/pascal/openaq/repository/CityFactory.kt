package com.pascal.openaq.repository

import com.pascal.openaq.city.network.model.City
import java.util.concurrent.atomic.AtomicInteger

class CityFactory {


    companion object {
        private val counter = AtomicInteger(1)
        fun createCity(): City {
            val id = counter.incrementAndGet()
            return City(
                    name = "name_$id",
                    country = "US",
                    count = 10000
            )
        }
        fun createListOfCity(size: Int)= List(size,{ createCity()})
    }
}