package com.pascal.openaq.repository

import com.pascal.openaq.city.network.model.RemoteAQService
import com.pascal.openaq.city.network.model.Response
import retrofit2.Call
import retrofit2.mock.Calls
import java.io.IOException

class FakeApi : RemoteAQService {
    var fails = false
    override fun requestCity(orderField: String, limit: Int, sortOrder: String, pageNumber: Int): Call<Response> {
        return if (fails) {
            Calls.failure<Response>(IOException("Test"))
        } else Calls.response(Response(Response.APIMeta("", 100, pageNumber), CityFactory.createListOfCity(limit)))
    }

}
